var gulp 			= require('gulp');
var sass 			= require('gulp-sass');
var postcss 		= require('gulp-postcss');
var autoprefixer 	= require('autoprefixer');
var sourcemaps   	= require('gulp-sourcemaps');
var runseq   		= require('run-sequence');
var cleanCSS 		= require('gulp-clean-css');
var cfg 			= require('../package.json').config;

gulp.task('sass', function () {
  return gulp.src(cfg.src.sass + '/**/*.{scss,sass}')
  	.pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(cfg.build.css));
});

gulp.task('sass:build', function () {
  return gulp.src(cfg.src.sass + '/**/*.{scss,sass}')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(cleanCSS())
    .pipe(gulp.dest(cfg.build.css));
});
 
gulp.task('sass:watch', function () {
  gulp.watch(cfg.src.sass + '/**/*.{scss,sass}', ['copy','sass'])
});