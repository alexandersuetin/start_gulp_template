var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function() {
	runSequence(
		'copy',
        'sass',
        'server',
        'javascript',
        'copy:watch',
        'sass:watch',
        'javascript:watch'
    );
});