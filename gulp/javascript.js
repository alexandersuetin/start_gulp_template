var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cfg = require('../package.json').config;
 
gulp.task('javascript', function () {
   return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'src/js/app.js'
		])
	.pipe(concat('common.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('build/js'));
});
 
gulp.task('javascript:watch', function () {
  gulp.watch(cfg.src.js + '/**/*.js', ['javascript']);
});